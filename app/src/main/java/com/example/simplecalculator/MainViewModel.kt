package com.example.simplecalculator

import androidx.compose.runtime.MutableState
import androidx.compose.ui.graphics.Color

import androidx.lifecycle.ViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow

class MainViewModel : ViewModel() {

    private val _result = MutableStateFlow(0.0)
    val result : StateFlow<Double> get() = _result

    private val _statusMessage = MutableStateFlow("Status:")
    val statusMessage_ : StateFlow<String> get() = _statusMessage

    private val _statusColor = MutableStateFlow(Color.Gray)
    val statusColor : StateFlow<Color> get() = _statusColor

    private val _operation = MutableStateFlow("add")
    val operation_ : StateFlow<String> get() = _operation


    fun setOperation(op_: String)
    {
        _operation.value = op_
    }

    fun setStatus(msg_ : String, color_ : Color) : Unit
    {
        _statusMessage.value = msg_
        _statusColor.value = color_

    }
 fun  calculateValues(val1: Int, val2: Int): Unit
    {
       _result.value = when(_operation.value)
        {
            "add" -> { val1.toDouble() + val2.toDouble()}
            "subtract" -> {val1.toDouble() - val2.toDouble()}
            "divide" -> {val1.toDouble() / val2.toDouble()}
            "multiply" -> {val1.toDouble() * val2.toDouble()}
            else -> { 0.0 }
        }
    }
}