package com.example.simplecalculator

import android.inputmethodservice.Keyboard
import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.activity.viewModels
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.wrapContentSize
import androidx.compose.foundation.text.BasicTextField
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material3.Button
import androidx.compose.material3.ButtonDefaults
import androidx.compose.material3.DropdownMenu
import androidx.compose.material3.DropdownMenuItem
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.material3.TextField
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.getValue
import androidx.compose.runtime.setValue
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.text.input.TextFieldValue
import androidx.compose.ui.tooling.preview.Preview
import com.example.simplecalculator.ui.theme.SimpleCalculatorTheme
import java.lang.Character.isDigit

class MainActivity : ComponentActivity() {

    private val mainViewModel by viewModels<MainViewModel>()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            SimpleCalculatorTheme {
                val result: Double by mainViewModel.result.collectAsState()
                val message_ : String by mainViewModel.statusMessage_.collectAsState()
                val statusColor_ : Color by mainViewModel.statusColor.collectAsState()

                // A surface container using the 'background' color from the theme
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colorScheme.background
                ) {
                    Calculator(result, message_, statusColor_)
                }
            }
        }
    }




    @OptIn(ExperimentalMaterial3Api::class)
    @Preview(showBackground = true)
    @Composable
    fun Calculator(result_: Double = 0.0,message_ : String = "Status:",statusColor : Color = Color.Gray) {
        var input1_state by remember { mutableStateOf("") }
        var input2_state by remember { mutableStateOf("") }
        Column {

            Row {
                TextField( value = input1_state,
                    onValueChange = { input1_state = it.filter{ it.isDigit()} },
                    modifier = Modifier
                        .fillMaxWidth(.5f),
                    label = { Text("Input 1") }, keyboardOptions = KeyboardOptions(keyboardType = KeyboardType.Number))
                TextField(value = input2_state,
                    onValueChange = { input2_state = it.filter{ it.isDigit()} },
                    modifier = Modifier
                        .fillMaxWidth(1f),
                    label = { Text("Input 2") }, keyboardOptions = KeyboardOptions(keyboardType = KeyboardType.Number)

                )
            }
            Button(onClick = {
                var validCalculate = true
                if (input1_state == "" || input2_state == "")
                {
                    validCalculate = false
                    input1_state = ""
                    input2_state = ""
                    mainViewModel.setStatus("Provide Valid Inputs", Color.Red)
                }
               /* if (input1_state.toInt() ==  || input2_state.toInt() == null )
                {
                    validCalculate = false
                    input1_state = ""
                    input2_state = ""
                    mainViewModel.setStatus("Provide Valid Integers", Color.Red)
                }*/
                if (validCalculate) {
                    mainViewModel.calculateValues(input1_state.toInt(), input2_state.toInt())
                    input1_state = ""
                    input2_state = ""
                    mainViewModel.setStatus("Success", Color.Red)

                    }
                             }, colors = ButtonDefaults.textButtonColors(contentColor = Color.Blue), modifier = Modifier.fillMaxWidth().background(color = Color.LightGray), ){Text("Calculate")}
            Box(modifier = Modifier.fillMaxWidth().align(Alignment.CenterHorizontally)){Text(text = result_.toString())}
            Text(message_, color = statusColor)
            OperatorsDropDown()


        }
    }

    @Composable
    @OptIn(ExperimentalMaterial3Api::class)
    fun OperatorsDropDown() {
        var expanded by remember { mutableStateOf(false) }
        val items = listOf("Add +", "Subtract -", "Divide /", "Multiply *")
        var selectedIndex by remember { mutableStateOf(0) }
        Box(
            modifier = Modifier
                .fillMaxSize()
                .wrapContentSize(Alignment.TopStart)

        ) {
            Text(
                items[selectedIndex], modifier = Modifier
                    .fillMaxWidth()
                    .clickable(onClick = { expanded = true })

                    .background(Color.Gray)
            )
            DropdownMenu(
                expanded = expanded, onDismissRequest = { expanded = false },
                modifier = Modifier
                    .fillMaxWidth()
                    .align(alignment = Alignment.TopCenter)
                    .background(Color.LightGray)
            )
            {
                items.forEachIndexed { index, s ->
                    DropdownMenuItem(
                        text = { Text(text = s) },
                        onClick = {
                            selectedIndex = index
                            expanded = false
                            when(index)
                            {
                                0 -> {mainViewModel.setOperation("add")}
                                1 -> {mainViewModel.setOperation("subtract")}
                                2 -> {mainViewModel.setOperation("divide")}
                                3 -> {mainViewModel.setOperation("multiply")}
                            }
                        })

                }
            }
        }

    }

}
