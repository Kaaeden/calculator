package com.example.simplecalculator

import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.DisplayName
import org.junit.jupiter.api.Test

class MainViewModelTest {
    private val mainViewModel = MainViewModel()
    @Test
    @DisplayName("Addition")
    fun addValues(){
        //Given
        val val1 = 5
        val val2 = 7
        //When
        mainViewModel.setOperation("add")
        mainViewModel.calculateValues(val1, val2)
        //Then
        Assertions.assertEquals(12.0, mainViewModel.result.value)
    }
    @Test
    @DisplayName("Subtraction")
    fun subtractValues(){
        //Given
        val val1 = 7
        val val2 = 5
        //When
        mainViewModel.setOperation("subtract")
        mainViewModel.calculateValues(val1, val2)
        //Then
        Assertions.assertEquals(2.0, mainViewModel.result.value)
    }
    @Test
    @DisplayName("Division")
    fun divideValues(){
        //Given
        val val1 = 7
        val val2 = 5
        //When
        mainViewModel.setOperation("divide")
        mainViewModel.calculateValues(val1, val2)
        //Then
        Assertions.assertEquals(1.4, mainViewModel.result.value)
    }
    @Test
    @DisplayName("Multiply")
    fun multiplyValues(){
        //Given
        val val1 = 7
        val val2 = 5
        //When
        mainViewModel.setOperation("multiply")
        mainViewModel.calculateValues(val1, val2)
        //Then
        Assertions.assertEquals(35.0, mainViewModel.result.value)
    }
}